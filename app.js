const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.json({
    id: 2000,
    name: 'Iphone'
  })
})

app.get('/hellome', (req, res) => {
  res.send('Hello Sumsung')
})

app.listen(port, () => {
  console.log(`Example app listen on port ${port}`)
})
